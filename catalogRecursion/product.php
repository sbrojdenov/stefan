<?php 
	$pageTitle = 'Product';
	require_once('includes/header.php'); 
	require_once('config.php');  
  
	if(isset($_SESSION['is_logged']) && $_SESSION['is_logged'] == true) { 
?>
<form class="form-horizontal" role="form" method="post">
  <div class="form-group">
    <label for="inp" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
      <input type="text" name="productname" class="form-control" id="inputEmail3" placeholder="Name">
    </div>
  </div>
  <div class="form-group">
    <label for="input" class="col-sm-2 control-label">Price</label>
    <div class="col-sm-10">
      <input type="text" name="price" class="form-control" id="inputPassword3" placeholder="Price">
    </div>
  </div>
  <div class="form-group">
    <label for="input" class="col-sm-2 control-label">Quantity</label>
    <div class="col-sm-10">
      <input type="text" name="quantity" class="form-control" id="inputPassword3" placeholder="Quantity">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-10">
      <textarea name="description" class="form-control" rows="3"></textarea>
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Picture</label>
    <div class="col-sm-10">
      <input type="file" name="picture" id="inputPassword3" >
    </div>
  </div>
   <label class="checkbox-inline">Category
  	<select name="categoryId" class="form-control">         
        <?php while($row = mysqli_fetch_array($result)):?>
                  <option value=<?php echo $row['CategoryId'];?>><?php echo $row['name'];?> </option>;          
        <?php endwhile ?>    
   </select>
   </label>
   <label class="checkbox-inline">Vendor
  	<select name="vendorId" class="form-control">         
        <?php while($row1 = mysqli_fetch_array($result1)):?>
                 <option value="<?php echo $row1['VendorId'];?>"><?php echo $row1['name'];?></option>;		 
        <?php endwhile ?>    
   </select>
   </label>
   <label class="checkbox-inline">Admin
  	<select name="adminId" class="form-control">         
        <?php while($row2 = mysqli_fetch_array($result2)):?>
                  <option value="<?php echo $row2['AdminId'];?>"><?php echo $row2['email'];?> </option>;          
        <?php endwhile ?>    
   </select>
  </label>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button name="submit" type="submit" class="btn btn-default">ADD elements</button>
    </div>
  </div>
  
  
</form>
<?php
if(isset($_POST['submit'])){
$productName= mysql_real_escape_string($_POST['productname']);
$price= mysql_real_escape_string($_POST['price']);
$quantity= mysql_real_escape_string($_POST['quantity']);
$description= mysql_real_escape_string($_POST['description']);
$category= mysql_real_escape_string($_POST['categoryId']);
$vendor= mysql_real_escape_string($_POST['vendorId']);
$admin= mysql_real_escape_string($_POST['adminId']);
$picture=$_POST['picture'];

   $n=new db();
   $n->connect();  
   $n->insertProduct($productName, $price, $quantity, $description, $picture, $vendor, $category, $admin);
    
 }
 
?>
 <?php	
	}
	else {
		header('Location: login.php');
		exit;
	}