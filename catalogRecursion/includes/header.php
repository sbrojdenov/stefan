<?php session_start();  ?>
<!DOCTYPE html>
<html>
  <head>
    <title><?= $pageTitle; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">    
    <!-- Bootstrap -->
    <link href="includes/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="includes/bootstrap/css/style.css" rel="stylesheet"> 
  <style>
  ul li{
  list-style-type:none;
  }
  </style>  
  </head>
  <body>
    <div class="container"> 
		<?php if(isset($_SESSION['is_logged'])==  true) :?>
			<nav class="navbar navbar-default" role="navigation"> 
				<div class="navbar-header"> 
						<span class="navbar-brand" ><a href="index.php">CATALOG</a></span>
				</div>

				<ul class="nav navbar-nav">
					<li><a href="product.php" class="right">Product</a> </li> 
					<li><a href="vendor.php" class="right">Vendor</a> </li> 
					<li><a href="category.php" class="right">Category</a> </li>
					<li><a href="logout.php" class="right">Logout</a> </li> 
				</ul>  
			</nav>
		<?php endif; ?>