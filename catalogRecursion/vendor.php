<?php 
	$pageTitle = 'Vendor';
	require_once('includes/header.php'); 
	require_once('config.php');  
  
	if(isset($_SESSION['is_logged']) && $_SESSION['is_logged'] == true) { 
    ?>
  <form class="form-inline" role="form" method="POST">
  <div class="form-group">
    <label class="sr-only" for="exampleInputEmail2">Name</label>
    <input name="vendorname" type="text" class="form-control" id="exampleInputEmail2" placeholder="Enter name">
  </div>
  <button name="submit" type="submit" class="btn btn-default">Add</button>
  </form>
  <?php

   if(isset($_POST['submit'])){
      $name= mysql_real_escape_string($_POST['vendorname']);
   $n=new db();
    $n->connect();
    $n->insertVendor($name);
	}
  ?>
	
   <?php
	}
	else {
		header('Location: login.php');
		exit;
	}