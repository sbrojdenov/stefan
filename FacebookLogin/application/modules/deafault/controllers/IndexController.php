<?php

class Deafault_IndexController extends Zend_Controller_Action {

    protected $category;
    protected $data = array();
    protected $index = array();

    public function init() {
        /* Initialize action controller here */
    }

    public function indexAction() {

        $allAssetInfo = new Application_Model_Display();
        $allAssetInfo->getAssetInfo();
        $allAssetInfo->display_child_nodes(NULL, 0);
        $this->view->PritnTree = $allAssetInfo->resultStr;
    }

}

