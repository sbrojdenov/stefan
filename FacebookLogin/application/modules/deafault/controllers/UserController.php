<?php

class Deafault_UserController extends Zend_Controller_Action {

    public function init() {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('deafault/auth/index');
        }
    }

    public function indexAction() {
        $file = new Application_Model_User();
        $this->view->file = $file->fetchAll();
        $location=$file->fetchAll();
        foreach ($location as $value) {
            $mylocatio=($value['location']);
        }       
        $address = "Sofiq,Bulgaria";
        $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response);
        $lat = $response_a->results[0]->geometry->location->lat;    
        $long = $response_a->results[0]->geometry->location->lng;
        $this->view->lat=$lat;
        $this->view->long=$long;
    }

    public function logoutAction() {
        $auth = Zend_Auth::getInstance();
        $auth->clearIdentity();
        $this->_helper->redirector('index');
    }

}

