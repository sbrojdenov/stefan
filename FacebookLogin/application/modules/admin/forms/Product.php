<?php
class Admin_Form_Product extends Zend_Form
{
    public function init()
    {
        
    }

        public function __construct($options = null)
        {
        
        parent::__construct($options);
        
        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');
        
        $product = new Zend_Form_Element_Text('name');
        $product->setlabel('Product name');
        $product->setRequired('true');
        
        
        $price = new Zend_Form_Element_Text('price');
        $price->setlabel('price');
        $price->setRequired('true');
        
        
        $quantity = new Zend_Form_Element_Text('quantity');
        $quantity->setlabel('Quantity');
        $quantity->setRequired('true');
        
        
        $description = new Zend_Form_Element_Textarea('description');
        $description->setlabel('description');
        $description->setRequired('true');
        
        
        $picture = new Zend_Form_Element_File('picture');
        $picture->setlabel('Picture');
        $picture->setRequired('true');
        $picture->setLabel('Picture:')->setDestination(APPLICATION_PATH ."/../public/upload");
        
        
        $catId = new Admin_Form_Element_CategorySelect('CategoryId');
        $catId->setlabel('Categorys');
        $catId->setRequired('true');
        
        $vendorId = new Admin_Form_Element_VendorSelect('VendorId');
        $vendorId->setlabel('Vendors');
        $vendorId->setRequired('true');
        
        $adminId = new Admin_Form_Element_AdminSelect('AdminId');
        $adminId->setlabel('AdminId');
        $adminId->setRequired('true');
        
        $submit = new Zend_Form_Element_submit('submit');
        $submit->setlabel('submit');
        $submit->setRequired('true');
        
        
        $this->addElements(array($product,$price,
             $quantity,$description,$picture,$catId,$vendorId,$adminId,$submit));
        
        
    }
}