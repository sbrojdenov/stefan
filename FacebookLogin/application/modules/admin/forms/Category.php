<?php
class Admin_Form_Category extends Zend_Form
{
   public function init()
    {
        /* Form Elements & Other Definitions Here ... */
    }


        public function __construct($options = null)
        {
        
        parent::__construct($options);
        
        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');
        
        $category = new Zend_Form_Element_Text('category');
        $category->setlabel('Category');
        $category->setRequired('true');
        
        
        $parent = new Admin_Form_Element_CategorySelect('categoryId'); 
        $parent->setlabel('Subcategory');
        $parent->setRequired('true');
        
        
        $submit = new Zend_Form_Element_submit('submit');
        $submit->setlabel('submit');
        $submit->setRequired('true');
        
        
        $this->addElements(array($id, $category, $parent, $submit));
        
        
    }
}

