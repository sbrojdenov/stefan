<?php

class Admin_Form_Vendor extends Zend_Form {

    public function init() {
       
    }

    public function __construct($options = null) {

        parent::__construct($options);

        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');

        $name = new Zend_Form_Element_Text('name');
        $name->setlabel('Name');
        $name->setRequired('true');


        $submit = new Zend_Form_Element_submit('submit');
        $submit->setlabel('submit');
        $submit->setRequired('true');


        $this->addElements(array($id, $name,$submit));
    }

}
