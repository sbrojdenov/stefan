<?php

class Application_Model_DbTable_Vendor extends Zend_Db_Table_Abstract {

    protected $_name = 'vendor';
   

    public function getfile($id) {
        $id = (int) $id;

        $row = $this->fetchRow('VendorId = ' . $id);

        if (!$row) {
            throw new Exception("Error could not find row $id");
        }

        return $row->toArray();
    }

    public function addfile($name) {
        $data = array(
            'name' => $name,      
        );

        $this->insert($data);
    }

      public function updatefile($id, $name) {

        $data = array(
            'name' => $name,
        );

        $this->update($data, 'VendorId=' . (int) $id);
    }

    public function deletefile($id) {

        $this->delete('VendorId =' . (int) $id);
    }

}
