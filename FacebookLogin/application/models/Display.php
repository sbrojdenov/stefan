<?php

class Application_Model_Display extends Zend_Db_Table {

    private $dbAdapter;
    public $data = array();
    public $index = array();
    public $resultStr = "";

    public function __construct() {
        $this->dbAdapter = Zend_Db_Table::getDefaultAdapter();
    }

    public function getAssetInfo() {
        
        $select = new Zend_Db_Select($this->dbAdapter);

        $select->from(array('c' => 'category'), array('c.parentid','c.CategoryId','CategoryName'=>'c.name'));


        $select->joinLeft(array('p' => 'product'), 'c.CategoryId = p.CategoryId', array('ProductName'=>'p.name','p.price','p.description','p.quantity','p.picture'));

        $select->joinLeft(array('v' => 'vendor',), 'v.VendorId = p.VendorId', array('VendorName' => 'v.name'));

        $select->joinLeft(array('a' => 'admin'), 'a.AdminId = p.AdminId', array('email'));
         
//       $sql = $select->__toString();
//       echo "$sql\n";
        

        $stmt = $select->query();
        $result = $stmt->fetchAll();

       // return $result;
        foreach ($result as $row) {
            $id = $row["CategoryId"];
            $parent_id = $row["parentid"] === NULL ? "NULL" : $row["parentid"];
            $this->data[$id] = $row;
            $this->index[$parent_id][] = $id;
        }
    }
    function display_child_nodes($parent_id, $level) {
     
     $data = $this->data;
     $index = $this->index;
     $parent_id = $parent_id === NULL ? "NULL" : $parent_id;
     if (isset($index[$parent_id])) {
        foreach ($index[$parent_id] as $id) {           
             $this->resultStr .= '<ul><ul>';
             if($level==0){
             $this->resultStr .= '<li>';
             $this->resultStr .=  $data[$id]["VendorName"];
             $this->resultStr .='</li>';
             $this->resultStr .='<li>';
             $this->resultStr .=  $data[$id]["ProductName"];
             $this->resultStr .='</li>';
             $this->resultStr .='<li>';
             $this->resultStr .=  $data[$id]["price"];
             $this->resultStr .='</li>';
             $this->resultStr .='<li>';
             $this->resultStr .= $data[$id]["description"];
             $this->resultStr .='</li>';
             $this->resultStr .='<li>';
             $this->resultStr .= $data[$id]["quantity"];
             $this->resultStr .='</li>';
             $this->resultStr .='<li>';
             $this->resultStr .= $data[$id]["picture"];
             $this->resultStr .='</li>';
             }
             $this->resultStr .='<li>';
             $this->resultStr .= str_repeat("-", $level) . $data[$id]["CategoryName"] . "\n";
             $this->resultStr .='</li>';
             $this->resultStr .='</ul>';
             $this->resultStr .='</li>';
             $this->resultStr .= '</ul>';           
            // echo $data[$id]["CategoryName"].$data[$id]["price"].$data[$id]["VendorName"];
            $this->display_child_nodes($id, $level + 1);
        }
        $this->resultStr .= "<br/>";
    }
   }
}



    


