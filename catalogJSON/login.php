<?php 
	$pageTitle = 'Login';
	require_once('includes/header.php'); 
	require_once('config.php'); 
	$errorArray = array();
	
	if(isset($_SESSION['is_logged']) === true) {
        // go go login
       header('Location: index.php');
       exit;
    }
    else {	
        // get data and log = 1
        if(isset($_POST['submit'])) {
            $strUsername = trim($_POST['username']);
            $strPassword = trim($_POST['password']);
			
			if(empty($strUsername) || empty($strPassword)) {
				 $errorArray[] = 'Prazni poleta!';
			} 
			
			// sql query
            $sql = 'SELECT * FROM admin WHERE email="' . mysqli_real_escape_string($con,$strUsername) . '" AND password="'. mysqli_real_escape_string($con,$strPassword) .'"';
              
            $q = mysqli_query($con,$sql);
            if(mysqli_num_rows($q) == 1) {
                $row = mysqli_fetch_assoc($q);  
                $_SESSION['is_logged'] = true; 
                $_SESSION['user_info'] = $row; 
            } elseif(mysqli_fetch_row($q) == 0) {
                  $errorArray[] = 'Error!';
            }
        }
        
?>
<div class="row vertical-offset-100">
	<div class="col-md-4 col-md-offset-4">
		<!-- Error reporting -->
		<?php  if (count($errorArray)) : ?>
		<div class="alert alert-danger">
			<strong>Error!</strong>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<ul>
			   <?php  foreach ($errorArray as $error) : ?>
				<li>
					<?=$error?>
				</li> 
				<?php endforeach ?>
			</ul>
		</div>
		<?php endif; ?>
		 
		<!-- Login form -->
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Please sign in</h3>
			</div>
			<div class="panel-body">
				<form method="post" action="">
					<fieldset>
						   <div class="form-group">
							   <input class="form-control" placeholder="Username" name="username" type="text">
						   </div>
						   <div class="form-group">
								   <input class="form-control" placeholder="Password" name="password" type="password">
						   </div> 
						   <input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Login">
				   </fieldset>
				</form>
			</div>
		</div>
	</div>
</div> 
<?php } 
require_once('includes/footer.php');
	